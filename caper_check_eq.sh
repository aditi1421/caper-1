#!/bin/bash -e
# Caper: a pcap expression analysis utility.
# Nik Sultana, November 2018.

if [ -z "${Z3_BIN}" ]
then
  echo "Need to set \$Z3_BIN to the path of a Z3 binary" >&2
  exit 1
fi

TEMPLATE_FILE=template.smt
OUTPUT_FILE=$1
LHS=$2
RHS=$3

CAPER_PARAM=""

# Disabling expansion focuses testing on the SMT conversion stage only.
#CAPER_PARAM="-not_expand"

LHS=$(./caper.byte ${CAPER_PARAM} -r -q -smt_translate -e "${LHS}")
RHS=$(./caper.byte ${CAPER_PARAM} -r -q -smt_translate -e "${RHS}")

perl -e "while(<>){print;if(\$_=~/include the lhs here. DO NOT CHANGE THIS LINE./){print \"${LHS}\"}}" ${TEMPLATE_FILE} \
| perl -e "while(<>){print;if(\$_=~/include the rhs here. DO NOT CHANGE THIS LINE./){print \"${RHS}\"}}" \
> ${OUTPUT_FILE}

command time ${Z3_BIN} -smt2 ${OUTPUT_FILE}
