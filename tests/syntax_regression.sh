#!/bin/bash
#
# Caper: a pcap expression analysis utility.
# Regression tests for parsing/printing
# Nik Sultana, August 2015
#
# FIXME some failing tests are only failing because of whitespace mismatch -- e.g., "=" vs " = " -- or explicit bracketing.
# FIXME remove repeated tests.
# FIXME allow negative tests -- expressions that should not parse.
# FIXME translate syntax tests into disambiguate tests.

CAPER="$1"
TEST_SCRIPT="$2"

THIS_FILE=`basename "$0"`

function tst() {
  if [ "$#" -ne 2 ]
  then
    echo "Incorrect number of parameters passed to ${THIS_FILE}" >&2
    exit 1
  fi
  ${TEST_SCRIPT} "${CAPER}" "$2" "$1"
}

# These examples of pcap syntax are drawn from
#   https://www.wireshark.org/docs/man-pages/pcap-filter.html

tst "S1" "host foo"
tst "S2" "net 128.3"
tst "S3" "port 20"
tst "S4" "portrange 6000-6008"

tst "S5" "src foo"
tst "S6" "dst net 128.3"
tst "S7" "src or dst port ftp-data"

tst "S8" "ether src foo"
tst "S9" "arp net 128.3"
tst "S10" "tcp port 21"
tst "S11" "udp portrange 7000-7009"
tst "S12" "wlan addr2 0:2:3:4:5:6"
#tst "S13" "ether 00:02:03:04:05:06" # Test for using MAC adddress -- this test shouldn't pass since it must specify "host".
tst "S14" "ether host 00:02:03:04:05:06" # Test for using MAC adddress
tst "S15" "ether src host 00:02:03:04:05:06" # Test for using MAC adddress
# Related tests continue at S236

tst "S16" "src foo"
  # means same thing as
#tst "ip src foo or arp src foo or rarp src foo"
tst "S17" "ip src foo || arp src foo || rarp src foo"
tst "S18" "net bar"
  # means same thing as
#test "ip net bar or arp net bar or rarp net bar"
tst "S19" "ip net bar || arp net bar || rarp net bar"
tst "S20" "port 53"
  # means same thing as
#tst "tcp port 53 or udp port 53"
tst "S21" "tcp port 53 || udp port 53"

#tst "host foo and not port ftp and not port ftp-data"
tst "S22" "host foo && ! port ftp && ! port ftp-data"
#tst "tcp dst port ftp or ftp-data or domain"
tst "S23" "tcp dst port ftp || ftp-data || domain"
  # means same thing as
#tst "tcp dst port ftp or tcp dst port ftp-data or tcp dst port domain"
tst "S24" "tcp dst port ftp || tcp dst port ftp-data || tcp dst port domain"

tst "S25" "dst host \host"
tst "S26" "src host \host"
tst "S27" "host \host"
tst "S28" "ip host \host"
#tst "ether proto \ip and host \host"
tst "S29" "ether proto \ip && host \host"
tst "S30" "ether dst ehost"
tst "S31" "ether src ehost"
tst "S32" "ether host ehost"
tst "S33" "gateway \host"
#test "ether host ehost and not host \host"
tst "S34" "ether host ehost && ! host \host"
tst "S35" "dst net \net"
tst "S36" "dst net 192.168.1.0"
tst "S37" "dst net 192.168.1"
tst "S38" "dst net 172.16"
tst "S39" "dst net 10"
tst "S40" "dst net 192.168.1.0 mask 255.255.255.0"
tst "S41" "dst net 192.168.1 mask 255.255.255.0"
tst "S42" "dst net 172.16 mask 255.255.0.0"
tst "S43" "dst net 10 mask 255.0.0.0"
tst "S44" "dst net 192.168.1.0/24"
tst "S45" "dst net 192.168.1/24"
tst "S46" "dst net 172.16/16"
tst "S47" "dst net 10/8"
tst "S48" "dst port 513"
tst "S49" "dst port domain"
tst "S50" "src port \port"
tst "S51" "port \port"
tst "S52" "dst portrange port1-port2"
tst "S53" "src portrange port1-port2"
tst "S54" "portrange port1-port2"
tst "S55" "tcp src port \port"
#tst "less length" # FIXME "length" should be a number, not symbol
tst "S56" "less 10"
#tst "len <= length" # FIXME can "length" be a symbol, or only a number?
tst "S57" "len <= 400"
#tst "greater length" # FIXME "length" should be a number, not symbol
tst "S58" "greater 20"
#tst "len >= length" # FIXME can "length" be a symbol, or only a number?
tst "S59" "len >= 491"
tst "S60" "ip proto protocol"
tst "S61" "ip6 proto protocol"
tst "S62" "ip6 protochain protocol"
tst "S63" "ip6 protochain 6"
tst "S64" "ip protochain protocol"
tst "S65" "ether broadcast"
tst "S66" "ip broadcast"
tst "S67" "ether multicast"
tst "S68" "ether[0] & 1 != 0"
tst "S69" "ip multicast"
tst "S70" "ip6 multicast"
tst "S71" "ether proto protocol"
tst "S72" "fddi protocol arp"
tst "S73" "tr protocol arp"
tst "S74" "wlan protocol arp"
tst "S75" "decnet src host"
tst "S76" "ip[0] & 0xf != 5"
#tst "ip[6:2] & 0x1fff = 0"
tst "S77" "ip[6 : 2] & 0x1fff = 0" #NOTE the test-passed check is whitespace sensitive
#tst "not host vs and ace"
tst "S78" "! host vs && ace"
#tst "not host vs and host ace"
tst "S79" "! host vs && host ace"
#tst "not ( host vs or ace )"
tst "S80" "! (host vs || ace)"

tst "S81" "host sundown"
#tst "host helios and ( hot or ace )"
tst "S82" "host helios && (hot || ace)"
#tst "ip host ace and not helios"
tst "S83" "ip host ace && ! helios"
tst "S84" "net ucb-ether"
#tst "gateway snup and (port ftp or ftp-data)"
tst "S85" "gateway snup && (port ftp || ftp-data)"
#tst "ip and not net localnet"
tst "S86" "ip && ! net localnet"
#tst "tcp[tcpflags] & (tcp-syn|tcp-fin) != 0 and not src and dst net localnet"
#tst "tcp[tcpflags] & (tcp-syn|tcp-fin) != 0 && ! src && dst net localnet"
tst "S87" "tcp[tcpflags] & (tcp-syn | tcp-fin) != 0 && ! src && dst net localnet" #NOTE the test-passed check is whitespace sensitive
#tst "tcp port 80 and (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)"
#tst "tcp port 80 && (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)"
tst "S88" "tcp port 80 && (ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 != 0" #NOTE the test-passed check is whitespace sensitive
#tst "gateway snup and ip[2:2] > 576"
#tst "gateway snup && ip[2:2] > 576"
tst "S89" "gateway snup && ip[2 : 2] > 576" #NOTE the test-passed check is whitespace sensitive
#tst "ether[0] & 1 = 0 and ip[16] >= 224"
tst "S90" "ether[0] & 1 = 0 && ip[16] >= 224"
#tst "icmp[icmptype] != icmp-echo and icmp[icmptype] != icmp-echoreply"
tst "S91" "icmp[icmptype] != icmp-echo && icmp[icmptype] != icmp-echoreply"

tst "S92" "vlan"
tst "S93" "vlan 100"
tst "S94" "vlan && vlan 100"

# External query sample
tst "S95" "arp"
tst "S96" "arp[6:2] == 2 or icmp6[0] == 136"
#arp and tcp
tst "S97" "arp && tcp"
#arp or tcp
tst "S98" "arp || tcp"
tst "S99" "dst port 23"
tst "S100" "egp"
tst "S101" "ether"
#(ether[0:1] == 0x45 and ether[9:1] == 1) or (ether[2:2] == 0x0800 and ether[4:1]==0x45 and ether[13:1]=1)
tst "S102" "(ether[0:1] == 0x45 && ether[9:1] == 1) || (ether[2:2] == 0x0800 && ether[4:1]==0x45 && ether[13:1]=1)"
tst "S103" "ether proto"
tst "S104" "ether proto ip6"
tst "S105" "fddi"
tst "S106" "host"
tst "S107" "host 10.0.0.5"
tst "S108" "icmp"
tst "S109" "icmp6"
#icmp and udp port 53 or bootpc
tst "S110" "icmp && udp port 53 || bootpc"
#icmp or udp port 22
tst "S111" "icmp || udp port 22"
#icmp or udp port 53 and bootpc
tst "S112" "icmp || udp port 53 && bootpc"
#icmp or udp port 53 or bootpc
tst "S113" "icmp || udp port 53 || bootpc"
#icmp or (vlan and icmp)
tst "S114" "icmp || (vlan && icmp)"
tst "S115" "ip"
tst "S116" "ip6"
tst "S117" "ip6 proto tcp"
tst "S118" "ip6 tcp"
tst "S119" "ip[8]<64"
# tst "S120" "ip broadcast" # FIXME duplicates S66
# tst "S121" "ip multicast" # FIXME duplicates S69
tst "S122" "ip proto tcp"
tst "S123" "ip src 1.0.0.1"
tst "S124" "ip src net"
tst "S125" "ip tcp"
tst "S126" "ipv6 tcp"
tst "S127" "mpls"
tst "S128" "net"
tst "S129" "net 10.0.0.5"
tst "S130" "port 22"
tst "S131" "rarp"
tst "S132" "src port 23"
tst "S133" "tcp"
#tcp or udp
tst "S134" "tcp || udp"
tst "S135" "tcp port 8"
tst "S136" "tcp port 80"
tst "S137" "tcp port 80 && (ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 != 0"
tst "S138" "tcp src port 23"
tst "S139" "udp"
#udp port 53 or bootpc
tst "S140" "udp port 53 || bootpc"
tst "S141" "vlan 500 && vlan 200"
#vlan and vlan 200
tst "S143" "vlan && vlan 200"
tst "S144" "wlan"
#arp or udp
tst "S145" "arp || udp"
tst "S146" "bootpc"
tst "S147" "ether[12]=0x800"
tst "S148" "host 1.1.1.1"
#host 10.1.1.1 and tcp port 443"
tst "S149" "host 10.1.1.1 && tcp port 443"
tst "S150" "host 192.168.0.1"
#icmp and port 22
tst "S151" "icmp && port 22"
#icmp or bootp
tst "S152" "icmp || bootp"
#icmp or udp
tst "S153" "icmp || udp"
#icmp or udp port 53
tst "S154" "icmp || udp port 53"
#icmp or udp port 53 or bootpc and tcp
tst "S155" "icmp || udp port 53 || bootpc && tcp"
#icmp or udp port 53 or bootpc or host 127.0.0.1
tst "S156" "icmp || udp port 53 || bootpc || host 127.0.0.1"
#icmp or udp port 53 or bootpc or tcp
tst "S157" "icmp || udp port 53 || bootpc || tcp"
#inbound and proto 47
tst "S158" "inbound && proto 47"
#inbound and proto gre
tst "S159" "inbound && proto gre"
#ip and vlan
tst "S160" "ip && vlan"
#ipv4 or udp
tst "S161" "ipv4 || udp"
tst "S162" "net 192.0.0.0/16"
tst "S163" "net 192.0.0.0/24"
tst "S164" "net 192.0.0.0/32"
tst "S165" "net 192.0.0.0/8"
#not (vlan and udp port 514)
tst "S166" "! (vlan && udp port 514)"
tst "S167" "sctp"
#tcp and udp
tst "S168" "tcp && udp"
#tcp or udp port 53
tst "S169" "tcp || udp port 53"
#tcp or udp port 53 and 123
tst "S170" "tcp || udp port 53 && 123"
tst "S171" "tcp port 22"
tst "S172" "udp port 53"
#vlan 10 and (tcp or udp)
tst "S173" "vlan 10 && (tcp || udp)"
#vlan 10 and tcp or udp
tst "S174" "vlan 10 && tcp || udp"
#vlan and (port 80 or port 443)
tst "S175" "vlan && (port 80 || port 443)"
#vlan and ip and not port 443
tst "S176" "vlan && ip && ! port 443"
#vlan and port 80 or vlan and port 443
tst "S177" "vlan && port 80 || vlan || port 443"
tst "S178" "ipv6"
#not udp"
tst "S179" "! udp"
#tcp or udp port 53 or 123
tst "S180" "tcp || udp port 53 || 123"
#icmp or udp port 53 or 54 or bootpc
tst "S181" "icmp || udp port 53 || 54 || bootpc"
#type mgt and subtype probe-req
tst "S182" "type mgt and subtype probe-req"
tst "S183" "ether proto 0x0806"
tst "S184" "udp and dst port 53"
tst "S185" "udp dst port 53"
tst "S186" "arp opcode 1"
tst "S187" "ether[21] = 1"
tst "S188" "ether[21] > 1"
# tst "S189" "ether broadcast" # FIXME duplicates S65
#udp or icmp or arp
tst "S190" "udp || icmp || arp"
#udp or icmp or tcp
tst "S191" "udp || icmp || tcp"
#udp or icmp or tcp and not egp
tst "S192" "udp || icmp || tcp && ! egp"
#udp or icmp or tcp or egp
tst "S193" "udp || icmp || tcp || egp"
tst "S194" "vlan 12"
#host 192.168.0.1 and ip
tst "S195" "host 192.168.0.1 && ip"

# External query sample -- originally using and/or/not instead of &&/||/!
tst "S196" "ip && (udp[8:2] = 0x1111) || (icmp[8:2] = 0x1111) || (tcp[((tcp[12]&0xf0)>>2):2] = 0x1111)"
tst "S197" "(src net 1.1.1.0/24 && dst net 1.1.1.0/24) || (src net 1.1.1.0/24 && dst net 1.1.1.0/24)"
tst "S198" "!ip[1]==1 && ip[2]==0x2 && !(ip[3:4]!=0 && ((ip[4]==4 && ip[5]==0) || (ip[1]==1 && ip[2]==2)))"
tst "S199" "! (host 1.1.1.1) && ! (arp && arp[1] = 1 && arp[1] = 1 && arp[1] = 1 && arp[1:2] = 3)"
tst "S200" "(ether[0:1] == 0x1 && ether[1:1] == 1) || (ether[2:2] == 0x0800 && ether[1:1]==0x1 && ether[3:1]=1)"
tst "S201" "(! ether src 11:11:11:11:11:11) && vlan && (ether[1:2] & 0xfff != 2) && (ether[1:2] & 0xfff != 3) && (ether[1:2] & 0xfff != 4)"
tst "S202" "((vlan 2) || (net 1.1.1.0/24) || (src net 1.1.1.0/24 && dst net 1.1.1.0/24) || (src net 1.1.1.0/24 && dst net 1.1.1.0/24))"
tst "S203" "! ((vlan 2) || (net 1.1.1.0/24) || (src net 1.1.1.0/24 && dst net 1.1.1.0/24) || (src net 1.1.1.0/24 && dst net 1.1.1.0/24))"
tst "S204" "arp || (ether[1:4]=0x11223344 && ether[2:2]=0x1122) || (ether[3:2]=0x3322 && ether[4:2]=0x4422) || (ether[1:2]=0x1111 && ether[2:4]=0x11223344 && ether[4:2]=0x4422)"
tst "S205" "ether dst 11:11:11:11:11:11 && (arp || (ether[:4]=0x11223344 && ether[2:2]=0x2222) || (ether[2:2]=0x1111 && ether[4:2]=0x5555) || (ether[2:2]=0x1111 && ether[1:4]=0x11221122 && ether[2:2]=0x2211))"
# External query sample -- originally using and/or/not instead of &&/||/!
tst "S206" "tcp[0:2] = tcp[4:2] && tcp src port 1111 && tcp[8:4] = 0"
tst "S207" "(ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 != 0"
tst "S208" "tcp port 80 && tcp[((tcp[12:1] & 0xf0) >> 2):4] = 0x11223344"
tst "S209" "icmp[icmptype] != icmp-echo && icmp[icmptype] != icmp-echoreply"
tst "S210" "tcp port 80 && ((ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 != 0"
tst "S211" "! (vlan 200 || src host 1.1.1.1 || dst host 1.1.1.1)"
tst "S212" "(udp src port 67 && dst port 68) || (udp src port 68 && dst port 67)"
tst "S213" "tcp port 80 && (((ip[2:2] - ((ip[0]&0xf)<<2)) - ((tcp[12]&0xf0)>>2)) != 0)"
tst "S214" "tcp port 80 && (ip[2 : 2] - (ip[0] & 0xf) << 2) - (tcp[12] & 0xf0) >> 2 != 0"
tst "S215" "(udp[8:2]=0x1111) || (icmp[8:2]=0x1111) || (tcp[((tcp[12]&0xf0)>>2):2]=0x1111)"
# External query sample -- originally using and/or/not instead of &&/||/!
tst "S216" "tcp port 20 || 30 || 40 || 50 || 60 || 70"
tst "S217" "! ((vlan 200) || (net 192.168.0.0/24))"
tst "S218" "ether[12:2] = 0x806 || ether[12:2] = 0x1234"
tst "S219" "ether[2] = 0xffffffff && ether[0] = 0xffff"
tst "S220" "icmp6 && (ip6[40] == 1 || ip6[40] == 2)"
tst "S221" "ether[0] & 0x0f > 2 && ether[0] & 0xf0 < 250"
tst "S222" "!ip[1]==1 && ip[1]==0x2 && !(ip[3:4]!=0)"
tst "S223" "ether[0:2] = 0xffff && ether[2:4] = 0xffffffff"
tst "S224" "ip6 host fe80:0000:0000:0000:1111:11ff:fe11:1111"
tst "S225" "((ip[0] & 0xf) << 2) - (tcp[1] & 0xf0) >> 2 != 0"
tst "S226" "ip6 dst host fe80:0000:0000:0000:1111:11ff:fe11:1111"
tst "S227" "ip6 src host fe80:0000:0000:0000:1111:11ff:fe11:1111"
tst "S228" "tcp port 80 && (ip[2 : 2] - (ip[0] & 0xf) << 2) != 0"
tst "S229" "tcp[tcpflags] & (tcp-rst|tcp-ack) == (tcp-rst|tcp-ack)"
tst "S230" "tcp[0:2] = tcp[4:2] && src port 100 and tcp[8:4] = 0"
tst "S231" "tcp[0:2] = tcp[4:2] && tcp[8:4] = 0 and src port 200"
tst "S232" "tcp[0:2] = tcp[4:2] && src port 300 and tcp[8:4] == 0"
tst "S233" "tcp[0:2] = tcp[4:2] && tcp[8:4] == 0 and src port 400"
tst "S234" "port 80 && tcp[((tcp[12:1] & 0xf0) >> 2):4] = 0x12345678"
tst "S235" "tcp[0:2] = tcp[4:2] && tcp[8:4] = 0 && tcp src port 500"

# Related tests occurred earlier at S15
tst "S236" "host ::1"
tst "S237" "host 0::1"
tst "S238" "host 0:0::1"
tst "S239" "host 0::0:1"
tst "S240" "host 0:0:0:0:0:0::1"
tst "S241" "host 0:0:0:0:0:0:0:1"
tst "S242" "host 00:00:00:00:00:00:00:01"
tst "S243" "host 000:000:000:000:000:000:000:001"
tst "S244" "host 0000:0000:0000:0000:0000:0000:0000:0001"
tst "S245" "ether host 0:1:2:3:4:5:6:7" # like S14, but using IPv6 address. This test should fail since the "ether" predicate doesn't apply to an IPv6 address.
tst "S246" "ether src host 0:1:2:3:4:5:6:7" # like S15, but using IPv6 address. This test should fail since the "ether" predicate doesn't apply to an IPv6 address.
tst "S247" "host 0:1:2:3:4:5:6:7" # like S14, but using IPv6 address.
tst "S248" "src host 0:1:2:3:4:5:6:7" # like S15, but using IPv6 address.
tst "S249" "ip6 host 0:1:2:3:4:5:6:7" # like S247, but being explicit about using IPv6 address.
tst "S250" "ip6 src host 0:1:2:3:4:5:6:7" # like S248, but being explicit about using IPv6 address.
