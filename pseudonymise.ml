(*
  Copyright Nik Sultana, February 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.


  This module: Pseudonymising pcap expressions to facilitate sharing them across organisations.
*)

open Pcap_syntax

let init_hash_size = 100 (*FIXME const*)

let ident_prefix = "id"
let port_prefix = "port"
let str_prefix = "str"
let ip6_prefix = "ip6_"
let mac_prefix = "mac"

let current_nat = ref 0
let current_ident_n = ref 0
let current_hex_n = ref 0
let hash_for_nat : (int, int) Hashtbl.t = Hashtbl.create init_hash_size
let hash_for_ident : (string, int) Hashtbl.t = Hashtbl.create init_hash_size
let hash_for_hex : (string, int) Hashtbl.t = Hashtbl.create init_hash_size

let current_port = ref 0
let hash_for_port : (string, int) Hashtbl.t = Hashtbl.create init_hash_size
let current_str = ref 0
let hash_for_str : (string, int) Hashtbl.t = Hashtbl.create init_hash_size
let current_ipv4 = ref 0
let hash_for_ipv4 : (int, int) Hashtbl.t = Hashtbl.create init_hash_size
let current_ipv6 = ref 0
let hash_for_ipv6 : (string, int) Hashtbl.t = Hashtbl.create init_hash_size
let current_mac = ref 0
let hash_for_mac : (string, int) Hashtbl.t = Hashtbl.create init_hash_size

let retrieve_or_create (current : 'b ref) (table : ('a, 'b) Hashtbl.t) (x : 'a) : 'b =
  match Hashtbl.find_opt table x with
  | Some y -> y
  | None ->
      current := !current + 1;
      Hashtbl.add table x !current;
      !current

let pseudonymise_value_atom (va : value_atom) : value_atom =
  let pseudo_port s = port_prefix ^ string_of_int (retrieve_or_create current_port hash_for_port s) in
  let pseudo_ipv4OptOctet i =
    match i with
    | None -> None
    | Some i' -> Some (retrieve_or_create current_ipv4 hash_for_ipv4 i') in
  let pseudo_ipv6 addr = ip6_prefix ^ string_of_int (retrieve_or_create current_ipv6 hash_for_ipv6 addr) in
  let pseudo_mac addr = mac_prefix ^ string_of_int (retrieve_or_create current_mac hash_for_mac addr) in
  match va with
  | Nothing
(*
  | Or_Escaped_String _
*)
  | Escaped_String _ -> va
  | Port s ->
    Port (pseudo_port s)
  | Port_Range (s1, s2) ->
    Port_Range (pseudo_port s1, pseudo_port s2)
(*
  | Or_Port ss ->
    Or_Port (List.map pseudo_port ss)
  | Or_Port_Range rs ->
    Or_Port_Range (List.map (fun (s1, s2) -> (pseudo_port s1, pseudo_port s2)) rs)
*)
  | String s ->
    String (str_prefix ^ string_of_int (retrieve_or_create current_str hash_for_str s))
  | IPv4_Address (i1, i2, i3, i4) ->
    IPv4_Address (Aux.the (pseudo_ipv4OptOctet (Some i1)), Aux.the (pseudo_ipv4OptOctet (Some i2)), Aux.the (pseudo_ipv4OptOctet (Some i3)), Aux.the (pseudo_ipv4OptOctet (Some i4)))
  | IPv6_Address (orig_address, expanded_address) ->
    let pseudonimised = pseudo_ipv6 expanded_address in
    IPv6_Address (pseudonimised, pseudonimised)
  | IPv4_Network (i1, i2_opt, i3_opt, i4_opt, ms_opt) ->
    IPv4_Network (Aux.the (pseudo_ipv4OptOctet (Some i1)), pseudo_ipv4OptOctet i2_opt, pseudo_ipv4OptOctet i3_opt, pseudo_ipv4OptOctet i4_opt, ms_opt)
  | IPv4_Network_Masked (i1, i2_opt, i3_opt, i4_opt, mask) ->
    IPv4_Network_Masked (Aux.the (pseudo_ipv4OptOctet (Some i1)), pseudo_ipv4OptOctet i2_opt, pseudo_ipv4OptOctet i3_opt, pseudo_ipv4OptOctet i4_opt, mask)
  | IPv6_Network (_, expanded_prefix, prefix_len) ->
    let pseudonimised = pseudo_ipv6 expanded_prefix in
    IPv6_Network (pseudonimised, pseudonimised, prefix_len)
  | MAC_Address addr ->
    MAC_Address (pseudo_mac addr)
(*
  | Or_String ss ->
    Or_String (List.map (fun s -> str_prefix ^ string_of_int (retrieve_or_create current_str hash_for_str s)) ss)
  | Or_IPv4_Address addrs ->
    Or_IPv4_Address (List.map (fun (i1, i2, i3, i4) -> (Aux.the (pseudo_ipv4OptOctet (Some i1)), Aux.the (pseudo_ipv4OptOctet (Some i2)), Aux.the (pseudo_ipv4OptOctet (Some i3)), Aux.the (pseudo_ipv4OptOctet (Some i4)))) addrs)
  | Or_IPv6_Address addrs ->
    Or_IPv6_Address (List.map pseudo_ipv6 addrs)
  | Or_IPv6_Network ss ->
    Or_IPv6_Network (List.map (fun (s1, s2) -> (pseudo_ipv6 s1, pseudo_ipv6 s2)) ss)
  | Or_IPv4_Network nets ->
    Or_IPv4_Network (List.map (fun (i1, i2_opt, i3_opt, i4_opt, ms_opt) -> (Aux.the (pseudo_ipv4OptOctet (Some i1)), pseudo_ipv4OptOctet i2_opt, pseudo_ipv4OptOctet i3_opt, pseudo_ipv4OptOctet i4_opt, ms_opt)) nets)
  | Or_IPv4_Network_Masked nets ->
    Or_IPv4_Network_Masked (List.map (fun (i1, i2_opt, i3_opt, i4_opt, mask) -> (Aux.the (pseudo_ipv4OptOctet (Some i1)), pseudo_ipv4OptOctet i2_opt, pseudo_ipv4OptOctet i3_opt, pseudo_ipv4OptOctet i4_opt, mask)) nets)
  | Or_MAC_Address addrs ->
    Or_MAC_Address (List.map pseudo_mac addrs)
*)

let pseudonymise_prim (((proto_opt, dir_opt, typ_opt), value_atom) : primitive) : primitive =
  ((proto_opt, dir_opt, typ_opt), pseudonymise_value_atom value_atom)

let rec pseudonymise_e (e : expr) : expr =
  match e with
  | Identifier s ->
    Identifier (ident_prefix ^ string_of_int (retrieve_or_create current_ident_n hash_for_ident s))
  | Nat_Literal n ->
    Nat_Literal (retrieve_or_create current_nat hash_for_nat n)
  | Hex_Literal h ->
    Nat_Literal (retrieve_or_create current_hex_n hash_for_hex h)
  | Packet_Access (prot_s, e', nb_opt) ->
    Packet_Access (prot_s, pseudonymise_e e', nb_opt)
  | Len ->
    Len
  | Plus es ->
    Plus (List.map pseudonymise_e es)
  | Times es ->
    Times (List.map pseudonymise_e es)
  | Binary_And es ->
    Binary_And (List.map pseudonymise_e es)
  | Binary_Or es ->
    Binary_Or (List.map pseudonymise_e es)
  | Binary_Xor es ->
    Binary_Xor (List.map pseudonymise_e es)
  | Minus (e1, e2) ->
    Minus (pseudonymise_e e1, pseudonymise_e e2)
  | Quotient (e1, e2) ->
    Quotient (pseudonymise_e e1, pseudonymise_e e2)
  | Mod (e1, e2) ->
    Mod (pseudonymise_e e1, pseudonymise_e e2)
  | Shift_Right (e1, e2) ->
    Shift_Right (pseudonymise_e e1, pseudonymise_e e2)
  | Shift_Left (e1, e2) ->
    Shift_Left (pseudonymise_e e1, pseudonymise_e e2)

let rec pseudonymise_pe (pe : pcap_expression) : pcap_expression =
  match pe with
  | Primitive prim -> Primitive (pseudonymise_prim prim)
  | Atom re ->
      begin
        let re' =
          match re with
          | Gt (e1, e2) ->
            Gt (pseudonymise_e e1, pseudonymise_e e2)
          | Lt (e1, e2) ->
            Lt (pseudonymise_e e1, pseudonymise_e e2)
          | Geq (e1, e2) ->
            Geq (pseudonymise_e e1, pseudonymise_e e2)
          | Leq (e1, e2) ->
            Leq (pseudonymise_e e1, pseudonymise_e e2)
          | Eq (e1, e2) ->
            Eq (pseudonymise_e e1, pseudonymise_e e2)
          | Neq (e1, e2) ->
            Neq (pseudonymise_e e1, pseudonymise_e e2) in
        Atom re'
      end
  | And pes -> And (List.map pseudonymise_pe pes)
  | Or pes -> Or (List.map pseudonymise_pe pes)
  | Not pe' -> Not (pseudonymise_pe pe')
  | True -> True
  | False -> False
