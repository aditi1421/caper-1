#!/bin/bash
# Caper: a pcap expression analysis utility.
# Nik Sultana, November 2018.
#
# When run with CAPER_WITH_ENGLISH=1 then it will also require Angstrom.
#   This Caper feature was tested with Angstrom version 0.15.0.
#   To install, run opam install angstrom.
#   Github: https://github.com/inhabitedtype/angstrom

if ! command -v ocamlbuild >/dev/null; then
  opam init --quiet --safe
  eval $(opam env)
fi

DEFAULT_TARGET=caper.byte

if [ -z $1 ]
then
  TARGET=${DEFAULT_TARGET}
else
  TARGET="${1}"
fi

ANGSTROM_DEP=""
ENGLISH_DIR=""
ENGLISH_DEF=""
if [ -n "${CAPER_WITH_ENGLISH}" ]
then
  ANGSTROM_DEP="-package angstrom"
  ENGLISH_DIR="-I english"
  ENGLISH_DEF="-DCAPER_WITH_ENGLISH"
fi

m4 ${ENGLISH_DEF} caper.ml.m4 > caper.ml
m4 ${ENGLISH_DEF} html.ml.m4 > html.ml

echo "building ${TARGET}"
if [ -n "${CAPER_WITH_ENGLISH}" ]
then
  echo "... with English conversion support"
else
  echo "... without English conversion support"
fi

# NOTE could add -dont-catch-errors to have exceptions pass through catches.
ocamlbuild -cflag -g -lflag -g -tag thread -use-ocamlfind -use-menhir \
  -no-hygiene \
  -package str \
  ${ANGSTROM_DEP} \
  -I syntax \
  -I pcap_to_bpf \
  -I pcap_to_bpf/protocol \
  -I pcap_to_bpf/optimizer \
  ${ENGLISH_DIR} \
  ${TARGET}
