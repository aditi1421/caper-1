(*
  Copyright Hyunsuk Bang, April 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.


  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: BPF optimizer
*)

open Inst
open Block
open Register
open Util

let no_opify (op : opcode) =
  match op with
  | No_op op -> No_op op
  | other -> No_op other

let opify (op : opcode) =
  match op with
  | No_op op -> op
  | other -> other

let op_eq op1 op2 =
  match op1, op2 with
  | No_op x, No_op y -> x = y
  | No_op x, y | x, No_op y -> x = y
  | x, y -> x = y

let rec add_new_block_to_edge edges blk_key blk_eval =
  let new_e = blk_key :: (fst edges) in
  let new_eval = blk_eval :: (snd edges) in
  (new_e, new_eval)

let blocks_keys blocks = lst_map (fun b -> b.key) blocks

let get_pkt_acpt_line (blocks : block list) : int =
  let rec get_pkt_acpt_line_helper (ret : int) (blocks : block list) : int =
  match blocks with
  | b :: bs ->
    match b with
    | {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
    | {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1} -> ret
    | {key = key; opcodes = opcodes; cond = cond; jt = jt; jf = jf} ->
      get_pkt_acpt_line_helper (ret + List.length opcodes + 1) bs
  in
  get_pkt_acpt_line_helper 0 blocks

(* renumber blocks after certain changes *)
let renumber (blocks : block list) : block list =
  let line_map = Hashtbl.create (2 * List.length blocks) in
  let rec populate_line_map (line_cnt : int) (blocks : block list) : unit =
    match blocks with
    | [] -> ()
    | {key = k ; opcodes = [Ret True]; cond = c; jt = -1; jf = -1} :: [] ->
      Hashtbl.add line_map k line_cnt;
    | {key = k ; opcodes = [Ret True]; cond = c; jt = -1; jf = -1} :: bs ->
      Hashtbl.add line_map k line_cnt;
      populate_line_map (line_cnt + 1) bs
    | b :: bs ->
      Hashtbl.add line_map b.key line_cnt;
      populate_line_map (line_cnt + List.length b.opcodes + 1) bs
  in
  let renumber_helper (b : block) =
    match b with
    | {key = k ; opcodes = [Ret True]; cond = c; jt = -1; jf = -1} ->
      {key = Hashtbl.find line_map k; opcodes = [Ret True]; cond = c; jt = -1; jf = -1;}
    | {key = k; opcodes=[Ret False]; cond = c; jt = -1; jf = -1} ->
      {key = Hashtbl.find line_map k; opcodes = [Ret False]; cond = c; jt = -1; jf = -1;}
    | {key = k; opcodes = opcodes; cond = cond; jt = jt; jf = jf;} ->
      {
        key = Hashtbl.find line_map k;
        opcodes = opcodes;
        cond = cond;
        jt = Hashtbl.find line_map jt;
        jf = Hashtbl.find line_map jf;
      }
  in
  populate_line_map 0 blocks;
  lst_map renumber_helper blocks

let elim_neighbor_redun (blocks : block list) : block list =
  let pkt_acpt_line = ref 0 in
  let blocks_tbl = Hashtbl.create (2 * (List.length blocks)) in
  let rec init_blocks (line_cnt : int) (blocks : block list) : unit =
    match blocks with
    | [] -> ()
    | b :: bs ->
      match b with
      | {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
      | {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1} ->
        pkt_acpt_line := line_cnt;
        Hashtbl.add blocks_tbl b.key b;
        init_blocks line_cnt bs;
      | {key = key; opcodes = opcodes; cond = cond; jt = jt; jf = jf} ->
        Hashtbl.add blocks_tbl b.key b;
        init_blocks (line_cnt + List.length opcodes + 1) bs;
  in
  let del_child_redun (parent_ops : opcode list) (child_ops : opcode list) =
    let rec eval a_reg x_reg opcodes =
      match opcodes with
      | [] -> a_reg, x_reg
      | op :: ops ->
        let regs = eval_opcode a_reg x_reg op in
        eval (fst regs) (snd regs) ops
    in
    let rec del_child_redun_helper (ret : opcode list) (parent_ops : opcode list) (child_ops : opcode list) =
      match parent_ops, child_ops with
      | [], [] | _, [] -> lst_rev ret
      | [], x -> lst_rev ret @ x
      | p :: ps, c :: cs ->
        if op_eq p c then
          del_child_redun_helper (no_opify c :: ret) ps cs
        else
          lst_append (lst_rev ret) child_ops
    in
    let parent_regs = eval NilReg NilReg (List.map opify parent_ops) in
    let child_regs = eval NilReg NilReg child_ops in
    let child_ops' = del_child_redun_helper [] parent_ops child_ops in
    let child_regs' = eval (fst parent_regs) (snd parent_regs) child_ops' in
    if child_regs = child_regs' then
      child_ops'
    else
      child_ops
  in
  let rec elim_neighbor_redun_helper (blocks : block list) : unit =
    match blocks with
    | [] -> ()
    | b :: bs ->
      match b with
      | {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
      | {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1} ->
        elim_neighbor_redun_helper bs
      | {key = key; opcodes = opcodes; cond = cond; jt = jt; jf = jf} ->
        if jt <> !pkt_acpt_line && jt <> !pkt_acpt_line + 1 then
          let jt_block = Hashtbl.find blocks_tbl jt in
          let new_jt_opcodes = del_child_redun opcodes (List.map opify jt_block.opcodes) in
          Hashtbl.replace blocks_tbl jt_block.key (replace_opcodes new_jt_opcodes jt_block);
        else ();
        if jf <> !pkt_acpt_line && jf <> !pkt_acpt_line + 1 then
          let jf_block = Hashtbl.find blocks_tbl jf in
          let new_jf_opcodes = del_child_redun opcodes (List.map opify jf_block.opcodes) in
          Hashtbl.replace blocks_tbl jf_block.key (replace_opcodes new_jf_opcodes jf_block);
        else ();
        elim_neighbor_redun_helper bs;
  in
  let delete_noop (b : block) : block =
    let noop_filter = function
      | No_op _ -> false
      | other -> true
    in
    match b with
    | {key = key; opcodes = opcodes; cond = cond; jt = jt; jf = jf} ->
      let new_opcodes = List.filter noop_filter opcodes in
      replace_opcodes new_opcodes b
  in
  init_blocks 0 blocks;
  elim_neighbor_redun_helper blocks;
  let new_blks = (lst_map (Hashtbl.find blocks_tbl) (blocks_keys blocks)) in
  let new_blks' = lst_map delete_noop new_blks in
  renumber new_blks'

let switch_order (blocks : block list) : block list =
  let pkt_acpt_line = get_blocks_length blocks in
  let rec renumber_acc (ret : block list) (acc : block list) =
    match acc with
    | [] -> ret
    | [c] -> renumber_acc (c :: ret) []
    | a :: c :: tl ->
      match a, c with
      | {key = a_key; opcodes = a_opcodes; cond = a_cond; jt = a_jt; jf = a_jf},
        {key = c_key; opcodes = c_opcodes; cond = c_cond; jt = c_jt; jf = c_jf} ->
          let new_a = {key = a_key; opcodes = a_opcodes; cond = a_cond; jt = a_jt; jf = a_jf} in
          let new_c_jf =
            if c_jf <> (pkt_acpt_line + 1) && c_jf <> pkt_acpt_line then
              a_jf + (List.length c_opcodes + 1)
            else c_jf
          in
          let new_c = {key = a_jf; opcodes = c_opcodes; cond = c_cond; jt = c_jt; jf = new_c_jf} in
          renumber_acc (a :: ret) (new_c :: tl)
    in
  let rec switch_order_helper (ret : block list) (acc1 : block list) (acc2 : block list) (blocks : block list) : block list =
    match blocks with
    | [] -> lst_rev ret
    | a :: [] -> switch_order_helper (a :: ret) [] [] []
    | a :: b :: [] -> switch_order_helper (b :: a :: ret) [] [] []
    | a :: b :: c :: [] -> switch_order_helper (c :: b :: a :: ret) [] [] []
    | a :: b :: c :: d :: tl ->
      match a, b, c, d with
      | {key = a_key; opcodes = a_opcodes; cond = a_cond; jt = a_jt; jf = a_jf},
        {key = b_key; opcodes = b_opcodes; cond = b_cond; jt = b_jt; jf = b_jf},
        {key = c_key; opcodes = c_opcodes; cond = c_cond; jt = c_jt; jf = c_jf},
        {key = d_key; opcodes = d_opcodes; cond = d_cond; jt = d_jt; jf = d_jf} ->
          if (a_jt = b_jt && b_jt = c_jt && c_jt = d_jt) &&
            (a_jf = b_key && b_jf = c_key && c_jf = d_key) &&
            (a_opcodes = c_opcodes && b_opcodes = d_opcodes && a_opcodes <> b_opcodes && c_opcodes <> d_opcodes) then
              switch_order_helper ret (a :: acc1) (b :: acc2) (c :: d :: tl)
          else if List.length acc1 = 0 && List.length acc2 = 0 then
            switch_order_helper (a :: ret) [] [] (b :: c :: d :: tl)
          else
            let new_acc1 = lst_rev (a :: acc1) in
            let new_acc2 = lst_rev (b :: acc2) in
            let new_acc = renumber_acc [] (lst_append new_acc1 new_acc2) in
            switch_order_helper (lst_append new_acc ret) [] [] (c :: d :: tl)
    in
    switch_order_helper [] [] [] blocks

let merge_arith (blocks : block list) : block list =
  let rec merge_arith_helper (ret : block list) (blocks : block list) =
    match blocks with
    | [] -> lst_rev ret
    | b :: bs ->
      match b with
      | {key = key; opcodes = []; cond = Jeq (Lit 0); jt = jt; jf = jf}
      | {key = key; opcodes = []; cond = Jeq (Hex 0); jt = jt; jf = jf}
      | {key = key; opcodes = []; cond = Jeq (Hexj 0); jt = jt; jf = jf} -> merge_arith_helper (b :: ret) bs
      | {key = key; opcodes = opcodes; cond = Jeq (Lit 0); jt = jt; jf = jf}
      | {key = key; opcodes = opcodes; cond = Jeq (Hex 0); jt = jt; jf = jf}
      | {key = key; opcodes = opcodes; cond = Jeq (Hexj 0); jt = jt; jf = jf} ->
        begin
          let rev_opcodes = lst_rev opcodes in
          match List.hd rev_opcodes with
          | And x ->
            let new_block = {key = key; opcodes = (lst_rev (List.tl rev_opcodes)); cond = Jset x; jt = jt; jf = jf} in
            merge_arith_helper (new_block :: ret) bs
          | Sub x ->
            let new_block = {key = key; opcodes = (lst_rev (List.tl rev_opcodes)); cond = Jeq x; jt = jt; jf = jf} in
            merge_arith_helper (new_block :: ret) bs
          | other -> merge_arith_helper (b :: ret) bs
        end
      | other -> merge_arith_helper (b :: ret) bs
  in
  merge_arith_helper [] blocks

let optimize (blocks : block list) : block list =
  let original_blocks = ref blocks in
  let blocks_tbl = Hashtbl.create (2 * (List.length blocks)) in
  let blocks_eval_tbl = Hashtbl.create (2 * (List.length blocks)) in
  let edge_tbl = Hashtbl.create 1024 in
  (*initialize Hashtbl*)
  let rec init_blocks a_reg x_reg (blocks : block list) : unit =
    match blocks with
    | [] -> ()
    | b :: bs ->
      match b with
      | {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
      | {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1} ->
        Hashtbl.add blocks_tbl b.key b;
        init_blocks NilReg NilReg bs;
      | {key = key; opcodes = opcodes; cond = cond; jt = jt; jf = jf} ->
        let eval_blk = eval_opcodes a_reg x_reg opcodes cond in
        Hashtbl.add blocks_tbl b.key b;
        Hashtbl.add blocks_eval_tbl b.key eval_blk;
        init_blocks (get_a_reg eval_blk) (get_x_reg eval_blk) bs;
  in
  (* initialize edge information *)
  let rec init_edge (blocks : block list) : unit =
    match blocks with
    | [] -> ()
    | b :: bs ->
      match b with
      | {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
      | {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1} -> ()
      | {key = key; opcodes = opcodes; cond = cond; jt = jt; jf = jf} ->
        let eval_blk = Hashtbl.find blocks_eval_tbl b.key in
        let cur_edge = Hashtbl.find_opt edge_tbl b.key in
        let jt_block_pred = Hashtbl.find_opt edge_tbl b.jt in
        let jf_block_pred = Hashtbl.find_opt edge_tbl b.jf in
        match cur_edge with
        | None ->
          begin
            match jt_block_pred, jf_block_pred with
            | None, None ->
              let new_jt_edge = ([b.key], [eval_blk]) in
              let new_jf_edge = ([b.key], [negate_reg_cond (eval_blk)]) in
              Hashtbl.add edge_tbl b.jt new_jt_edge;
              Hashtbl.add edge_tbl b.jf new_jf_edge;
              init_edge bs
            | Some jt_blk, None ->
              let new_jf_edge = ([b.key], [negate_reg_cond (eval_blk)]) in
              Hashtbl.add edge_tbl b.jf new_jf_edge;
              init_edge bs
            | None, Some jf_blk ->
              let new_jt_edge = ([b.key], [eval_blk]) in
              Hashtbl.add edge_tbl b.jt new_jt_edge;
              init_edge bs
            | Some jt_blk, Some jf_blk ->
              init_edge bs
          end
        | Some cur_edge ->
          begin
            match jt_block_pred, jf_block_pred with
            | None, None ->
              let new_jt_edge = add_new_block_to_edge cur_edge b.key eval_blk in
              let new_jf_edge = add_new_block_to_edge cur_edge b.key (negate_reg_cond eval_blk) in
              Hashtbl.add edge_tbl b.jt new_jt_edge;
              Hashtbl.add edge_tbl b.jf new_jf_edge;
              init_edge bs
            | Some jt_blk, None ->
              let new_jf_edge = add_new_block_to_edge cur_edge b.key (negate_reg_cond eval_blk) in
              Hashtbl.add edge_tbl b.jf new_jf_edge;
              init_edge bs
            | None, Some jf_blk ->
              let new_jt_edge = add_new_block_to_edge cur_edge b.key eval_blk in
              Hashtbl.add edge_tbl b.jt new_jt_edge;
              init_edge bs
            | Some jt_blk, Some jf_blk ->
              init_edge bs;
          end
  in

  (* merge duplicate blocks if possible *)
  let rec merge_blocks (blocks : block list) : unit =
    let new_jump_tbl = Hashtbl.create (2 * List.length !original_blocks) in
    let dup_tbl = Hashtbl.create (2 * List.length !original_blocks) in
    let can_merge (b1 : block) (b2 : block) : bool =
      if b1.opcodes = b2.opcodes && b1.cond = b2.cond && List.length (b1.opcodes) <> 0 then
        let b1_jt_blk = Hashtbl.find blocks_tbl b1.jt in
        let b2_jt_blk = Hashtbl.find blocks_tbl b2.jt in
        let b1_jf_blk = Hashtbl.find blocks_tbl b1.jf in
        let b2_jf_blk = Hashtbl.find blocks_tbl b2.jf in
        let cond1 = (b1_jt_blk = b2_jt_blk) || (b1_jt_blk.opcodes = b2_jt_blk.opcodes && b1_jt_blk.cond = b2_jt_blk.cond) in
        let cond2 = (b1_jf_blk = b2_jf_blk) || (b1_jf_blk.opcodes = b2_jf_blk.opcodes && b1_jf_blk.cond = b2_jf_blk.cond) in
        cond1 || cond2
      else false
    in
    let renumber_helper (b : block) : block =
      match b with
      | {key = k; opcodes = [Ret True]; cond = c; jt = -1; jf = -1}
      | {key = k; opcodes = [Ret False]; cond = c; jt = -1; jf = -1} -> b
      | {key = k; opcodes = opcodes; cond = cond; jt = jt; jf = jf;} ->
        {
          key = k;
          opcodes = opcodes;
          cond = cond;
          jt = if Hashtbl.mem new_jump_tbl jt then Hashtbl.find new_jump_tbl jt else jt;
          jf = if Hashtbl.mem new_jump_tbl jf then Hashtbl.find new_jump_tbl jf else jf;
        }
    in
    let rec merge_blocks_helper (acc : block list) (blocks : block list) : block list =
      match blocks with
      | [] -> failwith "Bpf_optimizer.merge_block_helper"
      | ret_true :: ret_false :: [] ->
        let new_blk = lst_rev (ret_false :: ret_true :: acc) in
        let new_blk' = lst_map renumber_helper new_blk in
        new_blk'
      | b :: bs ->
        match Hashtbl.find_opt dup_tbl (b.opcodes, b.cond) with
        | None ->  (* no dup find yet *)
          Hashtbl.add dup_tbl (b.opcodes, b.cond) b;
          merge_blocks_helper (b :: acc) bs
        | Some dup_blk ->
          if can_merge dup_blk b then
            begin
              Hashtbl.add new_jump_tbl dup_blk.key b.key;
              merge_blocks_helper (b :: List.filter (fun a -> a.key <> dup_blk.key )acc) bs
            end
          else
            merge_blocks_helper (b :: acc) bs
    in
    let new_blk = merge_blocks_helper [] blocks in
    if new_blk = blocks then
      original_blocks := blocks
    else
      merge_blocks new_blk
  in

  (* move blocks to remove edge inversion after merge_blocks*)
  let moved_blocks_tbl = Hashtbl.create (List.length blocks) in
  let rec elim_edge_inversion (acc : block list) (blocks : block list) : block list =
    let insert_block (acc : block list) (blk : block) : block list =
      let int_min x y = if x < y then x else y in (*NOTE using Int.min doesn't work with older versions of v4 OCaml compilers*)
      let target_key = int_min blk.jt blk.jf in
      let rec insert_block_helper (ret : block list) (rev_block : block list) : block list =
        match rev_block with
        | [] -> failwith "Bpf_optimizer.elim_blocks_inversion.insert_block_helper"
        | b :: bs ->
          if b.key = target_key then
            begin
              let new_blocks = lst_append (lst_rev bs) (blk :: b :: ret) in
              new_blocks;
            end
          else insert_block_helper (b :: ret) bs
      in
      insert_block_helper [] acc
    in
    match blocks with
    | [] ->
      let new_blocks = acc |> lst_rev |> renumber in
      if new_blocks <> !original_blocks then
        begin
          Hashtbl.clear moved_blocks_tbl;
          original_blocks := new_blocks;
          elim_edge_inversion [] !original_blocks
        end
      else
        new_blocks
    | b :: bs ->
      match b with
      | {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
      | {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1} ->
        elim_edge_inversion (b :: acc) bs
      | other ->
        if (b.key > b.jt || b.key > b.jf) && (not (Hashtbl.mem moved_blocks_tbl b)) then
          begin
            Hashtbl.add moved_blocks_tbl b 1;
            elim_edge_inversion [] (lst_append (insert_block acc b) bs)
          end
        else
          elim_edge_inversion (b :: acc) bs
  in

  (* dead_code_elimination *)
  let alive_keys = Hashtbl.create (2 * List.length blocks) in
  let rec dead_code_elim (blocks : block list) : unit =
    match blocks with
    | [] -> ()
    | b :: bs ->
      match b with
      | {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
      | {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1} ->
        let keys = Hashtbl.fold (fun k _ acc -> k :: acc) alive_keys [] in
        let new_blocks = lst_map (Hashtbl.find blocks_tbl) (0 :: (List.sort compare keys)) in
        if new_blocks = !original_blocks then
          ()
        else
          begin
            Hashtbl.clear alive_keys;
            original_blocks := new_blocks;
            dead_code_elim new_blocks
          end
      | other ->
        Hashtbl.replace alive_keys b.jt 1;
        Hashtbl.replace alive_keys b.jf 1;
        dead_code_elim bs
  in

  (* move edges based on the edge information *)
  let rec move_edges (blocks : block list) : unit =
    (* edge case: first block is not affected *)
    let rec move_first_blk_edges (blocks : block list) : unit =
      match blocks with
      | [] -> ()
      | b :: bs ->
        let jt_blk = Hashtbl.find blocks_tbl b.jt in
        let jf_blk = Hashtbl.find blocks_tbl b.jf in
        begin
          if b.opcodes = jt_blk.opcodes && b.cond = jt_blk.cond then
            let new_blk = b |> replace_jt jt_blk.jt in
            Hashtbl.replace blocks_tbl b.key new_blk
          else ();
        end;
        begin
          let jt_updated_blk = Hashtbl.find blocks_tbl b.key in
          if b.opcodes = jf_blk.opcodes && b.cond = jf_blk.cond then
            let new_blk = jt_updated_blk |> replace_jf jf_blk.jf in
            Hashtbl.replace blocks_tbl b.key new_blk
          else ();
        end
    in
    let rec move_edges_helper (blocks : block list) : unit =
      match blocks with
      | [] -> ()
      | b :: bs ->
        match b with
        | {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
        | {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1} -> ()
        | {key = key; opcodes = opcodes; cond = cond; jt = jt; jf = jf} ->
          match Hashtbl.find_opt edge_tbl b.key with
          | None ->
            let jt_blk = Hashtbl.find blocks_tbl b.jt in
            let jf_blk = Hashtbl.find blocks_tbl b.jf in
            begin
              if b.opcodes = jt_blk.opcodes && b.cond = jt_blk.cond then
                let new_blk = b |> replace_jt jt_blk.jt in
                Hashtbl.replace blocks_tbl b.key new_blk
              else ();
            end;
            begin
              if b.opcodes = jt_blk.opcodes && b.cond = jt_blk.cond then
                let new_blk = b |> replace_jt jt_blk.jt in
                Hashtbl.replace blocks_tbl b.key new_blk
              else ();
            end;
            move_edges_helper bs
          | Some edges ->
            let cur_blk_eval = Hashtbl.find blocks_eval_tbl b.key in
            let edges_cond = snd edges in
            let jt_blk = Hashtbl.find blocks_tbl b.jt in
            let jf_blk = Hashtbl.find blocks_tbl b.jf in
            match jt_blk, jf_blk with
            | {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1},
              {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1}
            | {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1},
              {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
            | {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1},
              {key = _; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
            | {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1},
              {key = _; opcodes = [Ret False]; cond = _; jt = -1; jf = -1} ->
              move_edges_helper bs
            | {key = _ ; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}, jf_blk
            | {key = _ ; opcodes = [Ret False]; cond = _; jt = -1; jf = -1}, jf_blk ->
              begin
                let insert_res = if_insert edges_cond (Hashtbl.find blocks_eval_tbl jf_blk.key) in
                match insert_res with
                | No_Collision ->
                  if b.opcodes = jf_blk.opcodes && b.cond = jf_blk.cond then
                    let new_blk = b |> replace_jf jf_blk.jf in
                    Hashtbl.replace blocks_tbl b.key new_blk
                  else ();
                  move_edges_helper bs
                | Triv_True ->
                  let new_blk = b |> replace_jf jf_blk.jt in
                  Hashtbl.replace blocks_tbl b.key new_blk;
                | Triv_False ->
                  let new_blk = b |> replace_jf jf_blk.jf in
                  Hashtbl.replace blocks_tbl b.key new_blk;
              end
            | jt_blk, {key = _ ; opcodes = [Ret True]; cond = _; jt = -1; jf = -1}
            | jt_blk, {key = _ ; opcodes = [Ret False]; cond = _; jt = -1; jf = -1} ->
              begin
                let insert_res = if_insert edges_cond (Hashtbl.find blocks_eval_tbl jt_blk.key) in
                match insert_res with
                | No_Collision ->
                  if b.opcodes = jt_blk.opcodes && b.cond = jt_blk.cond then
                    let new_blk = b |> replace_jt jt_blk.jt in
                    Hashtbl.replace blocks_tbl b.key new_blk
                  else ();
                  move_edges_helper bs
                | Triv_True ->
                  let new_blk = b |> replace_jt jt_blk.jt in
                  Hashtbl.replace blocks_tbl b.key new_blk;
                | Triv_False ->
                  let new_blk = b |> replace_jt jt_blk.jf in
                  Hashtbl.replace blocks_tbl b.key new_blk;
              end
            | jt_blk, jf_blk ->
              begin
                let insert_res = if_insert edges_cond (Hashtbl.find blocks_eval_tbl jt_blk.key) in
                match insert_res with
                | No_Collision ->
                  if b.opcodes = jt_blk.opcodes && b.cond = jt_blk.cond then
                    let new_blk = b |> replace_jt jt_blk.jt in
                    Hashtbl.replace blocks_tbl b.key new_blk
                  else ();
                | Triv_True ->
                  let new_blk = b |> replace_jt jt_blk.jt in
                  Hashtbl.replace blocks_tbl b.key new_blk;
                | Triv_False ->
                  let new_blk = b |> replace_jt jt_blk.jf in
                  Hashtbl.replace blocks_tbl b.key new_blk;
              end;
              begin
                let jt_updated_blk = Hashtbl.find blocks_tbl b.key in
                let insert_res = if_insert edges_cond (Hashtbl.find blocks_eval_tbl jf_blk.key) in
                match insert_res with
                | No_Collision ->
                  if b.opcodes = jf_blk.opcodes && b.cond = jf_blk.cond then
                    let new_blk = jt_updated_blk |> replace_jf jf_blk.jf in
                    Hashtbl.replace blocks_tbl b.key new_blk
                  else move_edges_helper bs;
                | Triv_True ->
                  let new_blk = jt_updated_blk |> replace_jf jf_blk.jt in
                  Hashtbl.replace blocks_tbl b.key new_blk;
                | Triv_False ->
                  let new_blk = jt_updated_blk |> replace_jf jf_blk.jf in
                  Hashtbl.replace blocks_tbl b.key new_blk;
              end;
    in
    move_first_blk_edges !original_blocks;
    move_edges_helper !original_blocks;
    let new_blocks = lst_map (Hashtbl.find blocks_tbl) (blocks_keys !original_blocks) in
    if new_blocks = !original_blocks then
      ()
    else
      begin
        original_blocks := new_blocks;
        Hashtbl.clear edge_tbl;
        init_edge !original_blocks;
        move_edges !original_blocks;
      end
  in

  original_blocks := blocks;
  init_blocks NilReg NilReg blocks;
  init_edge blocks;
  move_edges blocks;
  dead_code_elim !original_blocks;
  merge_blocks !original_blocks;
  !original_blocks
  |> merge_arith
  |> renumber
  |> elim_edge_inversion []
  |> switch_order
  |> elim_neighbor_redun
