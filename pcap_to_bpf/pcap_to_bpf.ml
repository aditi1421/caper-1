(*
  Copyright Hyunsuk Bang, December 2022 - April 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: Compiler from expanded pcap expressions to BPF
*)

open Infix_to_postfix
open Pcap_to_tree
open Tree_to_bpf
open Bpf_optimizer
open Block
open Util
open Protochain
open Icmp6

let pcap_to_bpf (pcap : string) : string =
  let postfix_pcap = pcap_infix_to_postfix pcap in
  if !Config.expand_linux_vlan && Str.string_match (Str.regexp "vlan.*") (List.hd postfix_pcap) 0 then
    let vlan_skf_ad_pcap = 
      match String.split_on_char ' ' (List.hd postfix_pcap) with
      | "vlan" :: [] -> "vlan SKF_AD" :: (List.tl postfix_pcap)
      | "vlan" :: [vlan_info] -> ("vlan SKF_AD_TAG " ^ vlan_info) :: (List.tl postfix_pcap)
    in
    let general_init = init_header postfix_pcap in
    let blk1 =
      pcaps_to_sftree (fst general_init) [] (snd general_init)
      |> sock_filter_tree_to_bpf
    in
    let vlan_skf_ad_init = init_header vlan_skf_ad_pcap in
    let blk2 =
      pcaps_to_sftree (fst vlan_skf_ad_init) [] (snd vlan_skf_ad_init)
      |> sock_filter_tree_to_bpf
    in
    disjoin_two_blocks blk1 blk2
    |> blocks_to_output
  else
    let init = init_header postfix_pcap in
    let main_bpf = 
      pcaps_to_sftree (fst init) [] (snd init)
      |> sock_filter_tree_to_bpf
    in
    let main_protochain_bpf = 
      let flattened_protochains = 
        Config.generate_optimized_bpf_output := false;
        !Protochain.protochain_asts
        |> assoc_lst_map sock_filter_tree_to_bpf
      in
      let flattened_icmp6_protochains = 
        Config.generate_optimized_bpf_output := false;
        !Icmp6.icmp6_protochain_asts
        |> assoc_lst_map sock_filter_tree_to_bpf
      in 
      let flattened_protochains_bridge =  
        Config.generate_optimized_bpf_output := false;
        !Protochain.protochain_bridge_asts 
        |> assoc_lst_map sock_filter_tree_to_bpf
      in
      insert_protochain_blocks main_bpf flattened_protochains flattened_icmp6_protochains flattened_protochains_bridge
      |> insert_jmp_blocks
    in
    blocks_to_output main_protochain_bpf
