let generate_protochain = ref false
let generate_icmp6_protochain = ref false
let protochain_list = ref []

let append_protochain (n : int) =
  protochain_list := (n :: !protochain_list)

let reset_env () =
  begin
    generate_protochain := false;
    generate_icmp6_protochain := false;
    protochain_list := [];
  end
