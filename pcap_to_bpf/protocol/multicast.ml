(*
  Copyright Hyunsuk Bang, May 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: Multicast
*)

open Inst
open Headers
open Util

let multicast_to_sock_filter (multicast_info : string) : sock_filter option =
  Some {code = [Ldb (Off 0)]; cond = Jset (Hexj(0x1)); jt = ret_true; jf = ret_false;}
