(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: ether protocol
*)

open Inst
open Util
open Headers

let ether_type = function
  | "ip" -> 0x800
  | "ip6" -> 0x86dd
  | "arp" -> 0x806
  | "rarp" -> 0x8035
  | "mpls" -> 0x8847
  | _ -> failwith "Ether.ether_type"

let ether_proto (protocol : string) (headers : packet_headers): sock_filter option =
  match headers with
  | Nil ->  Some {code = [Ldh (Off 12)]; cond = Jeq (Hexj (ether_type protocol)); jt = ret_true; jf = ret_false;}
  | Frame (_, l2_5) | Packet (_, l2_5, _) | Segment (_, l2_5, _, _) ->
    Some {code = [Ldh (Off (12 + List.length l2_5 * 4))]; cond = Jeq (Hexj (ether_type protocol)); jt = ret_true; jf = ret_false;}
