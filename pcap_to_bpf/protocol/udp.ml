(*
  Copyright Hyunsuk Bang, January 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: udp
*)

open Port
open Portrange
open Inst
open Util
open Headers

let udp_to_sock_filter (udp_info : string list) (headers : packet_headers) : sock_filter option =
  match udp_info with
  | "src" :: "and" :: "dst" :: "port" :: [port_num] -> src_dst_port "and" "udp" port_num headers
  | "src" :: "or" :: "dst" :: "port" :: [port_num] -> src_dst_port "or" "udp" port_num headers
  | "src" :: "port" :: [port_num] -> src_port "udp" port_num headers
  | "dst" :: "port" :: [port_num] -> dst_port "udp" port_num headers
  | "src" :: "and" :: "dst" :: "portrange" :: [portrange] -> src_dst_portrange "and" "udp" portrange headers
  | "src" :: "or" :: "dst" :: "portrange" :: [portrange] -> src_dst_portrange "or" "udp" portrange headers
  | "src" :: "portrange" :: [portrange] -> src_portrange "udp" portrange headers
  | "dst" :: "portrange" :: [portrange] -> dst_portrange "udp" portrange headers
  | _ -> failwith "Udp.udp_to_sock_filter"
