(*
  Copyright Hyunsuk Bang, May 2023

  This file is part of Caper.

  Caper is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Caper is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Caper.  If not, see <https://www.gnu.org/licenses/>.

  This module: Broadcast
*)

open Inst
open Headers
open Util

let broadcast_to_sock_filter (broadcast_info : string) : sock_filter option =
  let sf1 = Some {
    code = [
      Ld (Off 2)
    ];
    cond = Jeq (Hexj(0xffffffff));
    jt = ret_true;
    jf = ret_false;
  } in
  let sf2 = Some {
    code = [
      Ldh (Off 0)
    ];
    cond = Jeq (Hexj(0xffff));
    jt = ret_true;
    jf = ret_false;
  } in
  conjoin sf1 sf2
