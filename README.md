![Caper](doc/small_caper.jpg)

# Caper
Caper is a tool for understanding pcap expressions in the following ways:
* What does the pcap expression expand to "in full".
* Whether one expressions accepts all the packets accepted by the other (and possibly more).
* Whether the two expressions are equivalent (i.e., they accept/reject the same packets).

More info can be found in [the Caper paper](http://www.cs.iit.edu/~nsultana1/files/pcap_semantics.pdf).


# Building

Run `./build.sh`.

Instead of building in your immediate environment, you can use the
[Vagrantfile](Vagrantfile) that sets up a Debian 11 VM with the dependencies
that are needed to compile Caper, and then compiles it for you in that VM.


# Usage examples
Once Caper is compiled, running `./caper.byte` shows you a summary of command-line parameter behavior.

## Expanding pcap expressions "in full"

Caper can be used to turn the implicit meaning in pcap expressions into explicit syntax.

```
$ ./caper.byte -q -p -e "dst port http"

    ether proto \ip
    &&
    (ip proto \sctp && sctp dst port 80
      || ip proto \tcp && tcp dst port 80
      || ip proto \udp && udp dst port 80)
  ||
    ether proto \ip6
    &&
    (ip6 proto \sctp && sctp dst port 80
      || ip6 proto \tcp && tcp dst port 80
      || ip6 proto \udp && udp dst port 80)
```

```
$ ./caper.byte -q -p -e "src foo"
ether proto \arp && arp src host foo
  || ether proto \ip && ip src host foo
  || ether proto \ip6 && ip6 src host foo
  || ether proto \rarp && rarp src host foo
```

## Compiling pcap expressions to BPF

Caper's BPF compiler was written from scratch and provides better handling of encapsulated packets:

```
$ ./caper.byte -BPF -q -p -e "ip or ip6"
(000) ldh [12]
(001) jeq #0x800               jt 4      jf 2
(002) ldh [12]
(003) jeq #0x86dd              jt 4      jf 5
(004) ret #262144
(005) ret #0
```

It's also an optimizing compiler, for producing shorter codes:

```
$ ./caper.byte -BPF_optimized -q -p -e "ip src host 192.168.0.1 or ip src host 192.168.0.2"
(000) ldh [12]
(001) jeq #0x800               jt 2      jf 6
(002) ld [26]
(003) jeq #0xc0a80001          jt 5      jf 4
(004) jeq #0xc0a80002          jt 5      jf 6
(005) ret #262144
(006) ret #0
```

## Converting between pcap expressions and English

Use `-engl-in` to use English input:

```
$ ./caper.byte -engl-in -q -e "IPv4 that has a host of 192.168.0.2"
ip host 192.168.0.2
```

Use `-engl-out` to convert to English:

```
$ ./caper.byte -engl-out -q -e "tcp port 80 or 443"
tcp that has a port which is one of [80, 443]
```


# License
Caper is released under [GPLv3 or any later version](COPYING)


# Contributors

[Nik Sultana](https://www.nik.network/)   
[Hyunsuk Bang](https://hyunsuk-bang.github.io/resume/index.html)  
Denis Ovsienko  
Marelle Leon  
Jan Viktorin  
